#ExpClassifiers module
#Author: Federico Cruciani
#Description: Containes classes implementing CNN module with 1 and 5 channels.
from keras.models import Sequential
from keras.layers import *
import keras
from ExpBaseClassifier import ExpBaseClassifier

#Simple classifier Single thermal image (1 channel)
class HAR_CNN_IMU_4cl(ExpBaseClassifier):
	def __init__(self,patience,layers=3,num_classes=4,kern_size=2,divide_kernel_size=False,fontSize=16,suffix=""):
		self.name = "HAR_IMU_"+str(layers)+"-CNN_k"+str(kern_size)+"_SGD_dropout_"+suffix
		super().__init__(self.name,patience,fontSize)
		self.model = Sequential()
		filters = 12
		self.model.add( Dropout(0.2,input_shape=(120,6)) )
		self.model.add( Conv1D(filters,kernel_size=kern_size,padding='same',activation='relu', name="layer_1") )
		self.model.add( Dropout(0.5) )
		self.model.add(MaxPooling1D())
		for i in range(2,layers+1):
			filters = filters*2
			if divide_kernel_size:
				kern_size = int(kern_size / 2)
			layer_name = "layer_"+str(i)
			self.model.add( Conv1D(filters,kernel_size=kern_size,padding='same',activation='relu', name=layer_name) )
			self.model.add( Dropout(0.5) )
			self.model.add(MaxPooling1D())
		#Automatic features
		self.model.add(Flatten(name="automatic_features"))
		#for multilabel DO NOT use softmax use sigmoid
		self.model.add( Dense(64,activation='relu', name="layer_dense") )
		self.model.add( Dense(num_classes,activation='softmax',  name="output_layer"))
		self.model.compile( loss='mse',metrics=['mse','acc'], optimizer='sgd' )
		self.model.summary()
		self.name2layer = {}
		for layer in self.model.layers:
			self.name2layer[layer.name] = layer

	def fit_gen(self,uuids,epochs, val_data, imu_bg):
		spe = imu_bg.get_steps_per_epoch(uuids)
		self.history = self.model.fit_generator(imu_bg.batch_generator(uuids,epochs=epochs),epochs=epochs,steps_per_epoch=spe,validation_data=val_data,callbacks = [self.logger, self.early_stopping, self.checkpoint, self.csv_logger])

#name = fold_4_classes e.g., f0_4classes + _MLP_HCF_IMU
class HAR_HCF_IMU(ExpBaseClassifier):
	def __init__(self, patience, name="HAR", num_classes=4, fontSize=16, dropout=0.2,suffix=""):
		self.name = name + "_MLP_HCF_IMU_"+str(num_classes)+"classes_"+suffix
		super().__init__( self.name, patience, fontSize )
		self.model = Sequential()
		#consider input_dim=561 as parameter
		self.model.add( Dropout(dropout, input_shape=(44,)) )
		self.model.add( Dense( 128, activation='relu', name="input_layer" ) )
		self.model.add( Dense( 256, activation='relu', name="hidden_1" ) )
		self.model.add( Dense( 128, activation='relu', name="hidden_2" ) )
		self.model.add( Dense(64, activation='relu', name="hidden_3") )
		self.model.add( Dense( num_classes, activation='softmax', name="output_layer" ) )
		self.model.compile( loss='mse',metrics=['mse','acc'], optimizer='sgd' )
		self.model.summary()
		self.name2layer = {}
		for layer in self.model.layers:
			self.name2layer[layer.name] = layer

	def fit_gen(self,uuids,epochs, val_data, imu_bg):
		spe = imu_bg.get_steps_per_epoch(uuids)
		self.history = self.model.fit_generator(imu_bg.batch_generator(uuids,epochs=epochs),epochs=epochs,steps_per_epoch=spe,validation_data=val_data,callbacks = [self.logger, self.early_stopping, self.checkpoint, self.csv_logger])