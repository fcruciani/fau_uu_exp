import pandas as pd 
import numpy as np 
from extrautils import normalized
from extrautils import rawdatafolder
from sklearn import preprocessing
from random import randint
import extrautils
import keras.utils
import os

###

def get_steps_per_epoch(uuids):
	gt_df = pd.read_csv("GROUNDTRUTH_RSDATASET.csv",names=['UUID','label','steps','ts','class'])
	#exclude other uuids
	filtered_df = pd.DataFrame(columns=['UUID','label','steps','ts','class'])
	for uuid in uuids:
		data_uuid = gt_df[ gt_df['UUID'] == uuid ]
		filtered_df = pd.concat([filtered_df,data_uuid], ignore_index=True)

	#print("all data: ",len(gt_df))
	#print("only uuids: ", len(filtered_df))
	df_sit_all = filtered_df[filtered_df['label'] == "SITTING" ] #0
	df_wal_all = filtered_df[filtered_df['label'] == "WALKING" ] #1
	df_run_all = filtered_df[filtered_df['label'] == "RUNNING" ] #2
	df_cyc_all = filtered_df[filtered_df['label'] == "CYCLING" ] #3
	#df_sup_all = filtered_df[filtered_df['label'] == "STAIRS_UP" ] #4
	#df_sdo_all = filtered_df[filtered_df['label'] == "STAIRS_DOWN" ] #5
	#df_sta_all = filtered_df[filtered_df['label'] == "STANDING" ] #6
	#df_tra_all = filtered_df[filtered_df['label'] == "TRANSPORTATION" ] #7

	dic_sizes = [[0,len(df_sit_all)],[1,len(df_wal_all)],[2,len(df_run_all)],[3,len(df_cyc_all)]]
	print(dic_sizes)

	sizes = [len(df_sit_all), len(df_wal_all), len(df_run_all), len(df_cyc_all)] 	
	#print( "all: ", sizes, " - Min: ", min(sizes) )
	min_size = int(min(sizes)*10)
	return min_size


#@param uuid: list of UUIDs.
def batch_generator(uuids,epochs):
	step = 60
	gt_df = pd.read_csv("GROUNDTRUTH_RSDATASET.csv",names=['UUID','label','steps','ts','class'])
	#exclude other uuids
	filtered_df = pd.DataFrame(columns=['UUID','label','steps','ts','class'])
	for uuid in uuids:
		data_uuid = gt_df[ gt_df['UUID'] == uuid ]
		filtered_df = pd.concat([filtered_df,data_uuid], ignore_index=True)

	#print("all data: ",len(gt_df))
	#print("only uuids: ", len(filtered_df))
	df_sit_all = filtered_df[filtered_df['label'] == "SITTING" ] #0
	df_wal_all = filtered_df[filtered_df['label'] == "WALKING" ] #1
	df_run_all = filtered_df[filtered_df['label'] == "RUNNING" ] #2
	df_cyc_all = filtered_df[filtered_df['label'] == "CYCLING" ] #3
	#df_sup_all = filtered_df[filtered_df['label'] == "STAIRS_UP" ] #4
	#df_sdo_all = filtered_df[filtered_df['label'] == "STAIRS_DOWN" ] #5
	#df_sta_all = filtered_df[filtered_df['label'] == "STANDING" ] #6
	#df_tra_all = filtered_df[filtered_df['label'] == "TRANSPORTATION" ] #7

	dic_sizes = [[0,len(df_sit_all)],[1,len(df_wal_all)],[2,len(df_run_all)],[3,len(df_cyc_all)]]
	print(dic_sizes)

	sizes = [len(df_sit_all), len(df_wal_all), len(df_run_all), len(df_cyc_all) ] 	
	#print( "all: ", sizes, " - Min: ", min(sizes) )
	min_size = int(min(sizes)*10)

	for epochs in range(epochs):
		#one epoch finish afer min_size samples per class
		for i in range(min_size):
			X_list = []
			y_list = []
			#print("preparing batch")
			#1 batch 1 segement per class
			# about 30x7=210 samples per batch
			######## SITTING SAMPLES#############
			sit_fragment = df_sit_all.sample(n=1)
			#print(sit_fragment.iloc[0]['ts'])
			uuid = sit_fragment.iloc[0]['UUID']
			rawfile = rawdatafolder+uuid+"/"+ str(sit_fragment.iloc[0]['ts']) +".m_raw_acc.dat.gz"
			while not os.path.exists(rawfile):
				print("RAW DATA NOT FOUND: ",rawfile)
				sit_fragment = df_sit_all.sample(n=1)
				uuid = sit_fragment.iloc[0]['UUID']
				rawfile = rawdatafolder+uuid+"/"+ str(sit_fragment.iloc[0]['ts']) +".m_raw_acc.dat.gz"
			
			raw_data_df = pd.read_csv(rawfile,names=['ts','x','y','z'],sep='\s|,', compression='gzip',engine='python')
			raw_data_df = raw_data_df[ ['x','y','z'] ]
			if normalized[uuid] == False:
				raw_data_df.loc[:,['x']] = raw_data_df.loc[:,['x']].div(9.80665)
				raw_data_df.loc[:,['y']] = raw_data_df.loc[:,['y']].div(9.80665)
				raw_data_df.loc[:,['z']] = raw_data_df.loc[:,['z']].div(9.80665)

			start_index = 0
			end_index = 120
			while end_index < len(raw_data_df):
				X = raw_data_df[start_index:end_index].values
				y = 0
				y_list.append(y)
				X_list.append(X)
				#print( "X point ", start_index," : ",end_index, " Shape: ", X.shape )
				start_index += step
				end_index += step

			
			####################WALKING###########
			wal_fragment = df_wal_all.sample(n=1)
			#print(sit_fragment.iloc[0]['ts'])
			uuid = wal_fragment.iloc[0]['UUID']
			rawfile = rawdatafolder+uuid+"/"+ str(wal_fragment.iloc[0]['ts']) +".m_raw_acc.dat.gz"
			while not os.path.exists(rawfile):
				print("RAW DATA NOT FOUND: ",rawfile)
				wal_fragment = df_wal_all.sample(n=1)
				uuid = wal_fragment.iloc[0]['UUID']
				rawfile = rawdatafolder+uuid+"/"+ str(wal_fragment.iloc[0]['ts']) +".m_raw_acc.dat.gz"
			
			raw_data_df = pd.read_csv(rawfile,names=['ts','x','y','z'],sep='\s|,', compression='gzip',engine='python')
			
			raw_data_df = raw_data_df[ ['x','y','z'] ]
			if normalized[uuid] == False:
				raw_data_df.loc[:,['x']] = raw_data_df.loc[:,['x']].div(9.80665)
				raw_data_df.loc[:,['y']] = raw_data_df.loc[:,['y']].div(9.80665)
				raw_data_df.loc[:,['z']] = raw_data_df.loc[:,['z']].div(9.80665)

			start_index = 0
			end_index = 120
			while end_index < len(raw_data_df):
				X = raw_data_df[start_index:end_index].values
				y = 1
				y_list.append(y)
				X_list.append(X)
				#print( "X point ", start_index," : ",end_index, " Shape: ", X.shape )
				start_index += step
				end_index += step		
			##################RUNNING####################################
			run_fragment = df_run_all.sample(n=1)
			uuid = run_fragment.iloc[0]['UUID']
			rawfile = rawdatafolder+uuid+"/"+ str(run_fragment.iloc[0]['ts']) +".m_raw_acc.dat.gz"
			while not os.path.exists(rawfile):
				print("RAW DATA NOT FOUND: ",rawfile)
				run_fragment = df_run_all.sample(n=1)
				uuid = run_fragment.iloc[0]['UUID']
				rawfile = rawdatafolder+uuid+"/"+ str(run_fragment.iloc[0]['ts']) +".m_raw_acc.dat.gz"
			
			raw_data_df = pd.read_csv(rawfile,names=['ts','x','y','z'],sep='\s|,', compression='gzip',engine='python')
			
			raw_data_df = raw_data_df[ ['x','y','z'] ]
			if normalized[uuid] == False:
				raw_data_df.loc[:,['x']] = raw_data_df.loc[:,['x']].div(9.80665)
				raw_data_df.loc[:,['y']] = raw_data_df.loc[:,['y']].div(9.80665)
				raw_data_df.loc[:,['z']] = raw_data_df.loc[:,['z']].div(9.80665)

			rnd = randint(0,64)
			start_index = rnd
			end_index = 120+rnd
			while end_index < len(raw_data_df):
				X = raw_data_df[start_index:end_index].values
				y = 2
				y_list.append(y)
				X_list.append(X)
				#print( "X point ", start_index," : ",end_index, " Shape: ", X.shape )
				start_index += step
				end_index += step
			###############CYCLING############
			cyc_fragment = df_cyc_all.sample(n=1)
			uuid = cyc_fragment.iloc[0]['UUID']
			rawfile = rawdatafolder+uuid+"/"+ str(cyc_fragment.iloc[0]['ts']) +".m_raw_acc.dat.gz"
			while not os.path.exists(rawfile):
				print("RAW DATA NOT FOUND: ",rawfile)
				cyc_fragment = df_cyc_all.sample(n=1)
				rawfile = rawdatafolder+uuid+"/"+ str(cyc_fragment.iloc[0]['ts']) +".m_raw_acc.dat.gz"
			#loading raw data fragment
			raw_data_df = pd.read_csv(rawfile,names=['ts','x','y','z'],sep='\s|,', compression='gzip',engine='python')
			raw_data_df = raw_data_df[ ['x','y','z'] ]
			if normalized[uuid] == False:
				raw_data_df.loc[:,['x']] = raw_data_df.loc[:,['x']].div(9.80665)
				raw_data_df.loc[:,['y']] = raw_data_df.loc[:,['y']].div(9.80665)
				raw_data_df.loc[:,['z']] = raw_data_df.loc[:,['z']].div(9.80665)
			#extracting samples from raw data fragment
			rnd = randint(0,64)
			start_index = rnd
			end_index = 120+rnd
			while end_index < len(raw_data_df):
				X = raw_data_df[start_index:end_index].values
				y = 3
				y_list.append(y)
				X_list.append(X)
				start_index += step
				end_index += step


			#input("PAUSE")
			#extract_samples_from_fragment
			X_es = np.zeros((len(y_list),120,3))
			X_es[:,:] = [x for x in X_list  ]
			#Scaling input
			'''
			X_es_scaled = np.zeros((len(y_list),120,3))
			scaler_X = extrautils.load_classifier("./ACC_global_scaler_x")
			scaler_Y = extrautils.load_classifier("./ACC_global_scaler_y")
			scaler_Z = extrautils.load_classifier("./ACC_global_scaler_z")
			X_es_scaled[:,:,0] = scaler_X.transform(X_es[:,:,0])
			X_es_scaled[:,:,1] = scaler_Y.transform(X_es[:,:,1])
			X_es_scaled[:,:,2] = scaler_Z.transform(X_es[:,:,2])'''

			y_es = np.zeros(len(y_list))
			y_es[:] = [y for y in y_list]
			y_scaled = keras.utils.to_categorical(y_es, num_classes=4)
			#print("Batch size: ",len(y_list)," Unique classes: ",np.unique(y_es))
			#print(y_scaled)
			yield (X_es, y_scaled)


#get balanced subset
def get_balanced_dataset(uuids):
	gt_df = pd.read_csv("GROUNDTRUTH_RSDATASET.csv",names=['UUID','label','steps','ts','class'])
	#exclude other uuids
	filtered_df = pd.DataFrame(columns=['UUID','label','steps','ts','class'])
	for uuid in uuids:
		data_uuid = gt_df[ gt_df['UUID'] == uuid ]
		filtered_df = pd.concat([filtered_df,data_uuid], ignore_index=True)

	#print("all data: ",len(gt_df))
	#print("only uuids: ", len(filtered_df))
	df_sit_all = filtered_df[filtered_df['label'] == "SITTING" ] #0
	df_wal_all = filtered_df[filtered_df['label'] == "WALKING" ] #1
	df_run_all = filtered_df[filtered_df['label'] == "RUNNING" ] #2
	df_cyc_all = filtered_df[filtered_df['label'] == "CYCLING" ] #3
	
	dic_sizes = [[0,len(df_sit_all)],[1,len(df_wal_all)],[2,len(df_run_all)],[3,len(df_cyc_all)]]
	print(dic_sizes)

	sizes = [len(df_sit_all), len(df_wal_all), len(df_run_all), len(df_cyc_all) ] 	
	#print( "all: ", sizes, " - Min: ", min(sizes) )
	min_size = min(sizes)
	X_list = []
	y_list = []
	extractPoints(df_sit_all,min_size,X_list,y_list)
	print("Balanced set size: ",len(y_list))
	#extractPoints(df_sta_all,min_size,X_list,y_list)
	#print("Balanced set size: ",len(y_list))
	extractPoints(df_wal_all,min_size,X_list,y_list)
	print("Balanced set size: ",len(y_list))
	extractPoints(df_run_all,min_size,X_list,y_list)
	print("Balanced set size: ",len(y_list))
	extractPoints(df_cyc_all,min_size,X_list,y_list)
	print("Balanced set size: ",len(y_list))
	


	X_es = np.zeros((len(y_list),120,3))
	X_es[:,:] = [x for x in X_list  ]
	#Scaling input
	'''X_es_scaled = np.zeros((len(y_list),120,3))
	scaler_X = extrautils.load_classifier("./ACC_global_scaler_x")
	scaler_Y = extrautils.load_classifier("./ACC_global_scaler_y")
	scaler_Z = extrautils.load_classifier("./ACC_global_scaler_z")
	X_es_scaled[:,:,0] = scaler_X.transform(X_es[:,:,0])
	X_es_scaled[:,:,1] = scaler_Y.transform(X_es[:,:,1])
	X_es_scaled[:,:,2] = scaler_Z.transform(X_es[:,:,2])'''
	y_es = np.zeros(len(y_list))
	y_es[:] = [y for y in y_list]
	y_scaled = keras.utils.to_categorical(y_es, num_classes=4)
	return (X_es, y_scaled)


def get_test_dataset(uuids):
	gt_df = pd.read_csv("GROUNDTRUTH_RSDATASET.csv",names=['UUID','label','steps','ts','class'])
	#exclude other uuids
	filtered_df = pd.DataFrame(columns=['UUID','label','steps','ts','class'])
	for uuid in uuids:
		data_uuid = gt_df[ gt_df['UUID'] == uuid ]
		filtered_df = pd.concat([filtered_df,data_uuid], ignore_index=True)

	#print("all data: ",len(gt_df))
	#print("only uuids: ", len(filtered_df))
	df_sit_all = filtered_df[filtered_df['label'] == "SITTING" ] #0
	df_wal_all = filtered_df[filtered_df['label'] == "WALKING" ] #1
	df_run_all = filtered_df[filtered_df['label'] == "RUNNING" ] #2
	df_cyc_all = filtered_df[filtered_df['label'] == "CYCLING" ] #3
	
	dic_sizes = [[0,len(df_sit_all)],[1,len(df_wal_all)],[2,len(df_run_all)],[3,len(df_cyc_all)]]
	print(dic_sizes)

	sizes = [len(df_sit_all), len(df_wal_all), len(df_run_all), len(df_cyc_all) ] 	
	#print( "all: ", sizes, " - Min: ", min(sizes) )
	min_size = min(sizes)
	X_list = []
	y_list = []
	if len(df_sit_all) > 0:
		extractPoints(df_sit_all,int(len(df_sit_all)),X_list,y_list)
		print("Balanced set size: ",len(y_list))
	#extractPoints(df_sta_all,min_size,X_list,y_list)
	#print("Balanced set size: ",len(y_list))
	if len(df_wal_all) > 0:
		extractPoints(df_wal_all,len(df_wal_all),X_list,y_list)
		print("Balanced set size: ",len(y_list))
	if len(df_run_all) > 0:
		extractPoints(df_run_all,len(df_run_all),X_list,y_list)
		print("Balanced set size: ",len(y_list))
	if len(df_cyc_all) > 0:
		extractPoints(df_cyc_all,len(df_cyc_all),X_list,y_list)
		print("Balanced set size: ",len(y_list))
	


	X_es = np.zeros((len(y_list),120,3))
	X_es[:,:] = [x for x in X_list  ]
	
	y_es = np.zeros(len(y_list))
	y_es[:] = [y for y in y_list]
	y_scaled = keras.utils.to_categorical(y_es, num_classes=4)
	return (X_es, y_scaled)


def extractPoints(gt_dataframe,num_points,X_list,y_list):
	datapoints = gt_dataframe.sample(n=num_points)
	for index, fragment in datapoints.iterrows():
		uuid = fragment['UUID']
		rawfile = rawdatafolder+uuid+"/"+ str(fragment['ts']) +".m_raw_acc.dat.gz"
		while not os.path.exists(rawfile):
			print("RAW DATA NOT FOUND: ",rawfile)
			fragment = gt_dataframe.sample(n=1)
			rawfile = rawdatafolder+uuid+"/"+ str(fragment['ts']) +".m_raw_acc.dat.gz"
		#loading raw data fragment
		raw_data_df = pd.read_csv(rawfile,names=['ts','x','y','z'],sep='\s|,', compression='gzip',engine='python')
		raw_data_df = raw_data_df[ ['x','y','z'] ]
		if normalized[uuid] == False:
			raw_data_df.loc[:,['x']] = raw_data_df.loc[:,['x']].div(9.80665)
			raw_data_df.loc[:,['y']] = raw_data_df.loc[:,['y']].div(9.80665)
			raw_data_df.loc[:,['z']] = raw_data_df.loc[:,['z']].div(9.80665)
		#extracting samples from raw data fragment
		start_index = 0
		end_index = 120
		while end_index < len(raw_data_df):
			X = raw_data_df[start_index:end_index].values
			y = fragment['class']
			y_list.append(y)
			X_list.append(X)
			start_index += 60
			end_index += 60
	return