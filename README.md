# fau_uu_exp
CNN classifier using IMU (ACC+GYRO).
Classes = [sit, walk, run, cycle]

#Dependencies
Main Dependencies: numpy, pandas, sklearn, keras.

#Configuration:
edit datapath variable in 'test_fragments_4classes.py' with the 
actual path of the dataset.

 

#Running
launch 'test_fragments_4classes.py' to evaluate trained model:
	- this uses weights saved in 
	  './keras_logs/'+modelname+'_best.hdf5'


#NOTE: a pre-trained model is available in 'keras_logs'.


