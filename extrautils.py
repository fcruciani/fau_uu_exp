import pickle
import numpy as np
import pandas as pd
import gzip
import os
import glob
import detect_peaks
import scipy.fftpack
from scipy import signal
from scipy.stats import skew,kurtosis

#removed '665514DE-49DC-421F-8DCB-145D0B2609AD','C48CE857-A0DD-4DDB-BEA5-3A25449B2153',
done_uuids = ['74B86067-5D4B-43CF-82CF-341B76BEA0F4','59818CD2-24D7-4D32-B133-24C2FE3801E5','B09E373F-8A54-44C8-895B-0039390B859F','D7D20E2E-FC78-405D-B346-DBD3FD8FC92B','9DC38D04-E82E-4F29-AB52-B476535226F2','F50235E0-DD67-4F2A-B00B-1F31ADA998B9','0BFC35E2-4817-4865-BFA7-764742302A2D','4FC32141-E888-4BFF-8804-12559A491D8C','A76A5AF5-5A93-4CF2-A16E-62353BB70E8A','098A72A5-E3E5-4F54-A152-BBDA0DF7B694','797D145F-3858-4A7F-A7C2-A4EB721E133C','99B204C0-DD5C-4BB7-83E8-A37281B8D769','1DBB0F6F-1F81-4A50-9DF4-CD62ACFA4842','24E40C4C-A349-4F9F-93AB-01D00FB994AF','33A85C34-CFE4-4732-9E73-0A7AC861B27A','8023FE1A-D3B0-4E2C-A57A-9321B7FC755F','5152A2DF-FAF3-4BA8-9CA9-E66B32671A53','78A91A4E-4A51-4065-BDA7-94755F0BB3BB','5119D0F8-FCA8-4184-A4EB-19421A40DE0D','81536B0A-8DBF-4D8A-AC24-9543E2E4C8E0','9759096F-1119-4E19-A0AD-6F16989C7E1C','61359772-D8D8-480D-B623-7C636EAD0C81','E65577C1-8D5D-4F70-AF23-B3ADB9D3DBA3','BE3CA5A6-A561-4BBD-B7C9-5DF6805400FC']
done_normalized = {'E65577C1-8D5D-4F70-AF23-B3ADB9D3DBA3':True,'BE3CA5A6-A561-4BBD-B7C9-5DF6805400FC':True,'C48CE857-A0DD-4DDB-BEA5-3A25449B2153':True,'61359772-D8D8-480D-B623-7C636EAD0C81':False,'9759096F-1119-4E19-A0AD-6F16989C7E1C':True,'665514DE-49DC-421F-8DCB-145D0B2609AD':False,'81536B0A-8DBF-4D8A-AC24-9543E2E4C8E0':False,'8023FE1A-D3B0-4E2C-A57A-9321B7FC755F':True,'5152A2DF-FAF3-4BA8-9CA9-E66B32671A53':True,'5119D0F8-FCA8-4184-A4EB-19421A40DE0D':True,'78A91A4E-4A51-4065-BDA7-94755F0BB3BB':True,'33A85C34-CFE4-4732-9E73-0A7AC861B27A':False,'24E40C4C-A349-4F9F-93AB-01D00FB994AF':True,'1DBB0F6F-1F81-4A50-9DF4-CD62ACFA4842':False,'0A986513-7828-4D53-AA1F-E02D6DF9561B':True,'797D145F-3858-4A7F-A7C2-A4EB721E133C':False,'99B204C0-DD5C-4BB7-83E8-A37281B8D769':False,'098A72A5-E3E5-4F54-A152-BBDA0DF7B694':True,'74B86067-5D4B-43CF-82CF-341B76BEA0F4':False,'4FC32141-E888-4BFF-8804-12559A491D8C':True,'59818CD2-24D7-4D32-B133-24C2FE3801E5':False, 'D7D20E2E-FC78-405D-B346-DBD3FD8FC92B':True,'F50235E0-DD67-4F2A-B00B-1F31ADA998B9':False,'9DC38D04-E82E-4F29-AB52-B476535226F2':True,'99B204C0-DD5C-4BB7-83E8-A37281B8D769':False, '4FC32141-E888-4BFF-8804-12559A491D8C':True,'098A72A5-E3E5-4F54-A152-BBDA0DF7B694':True,'74B86067-5D4B-43CF-82CF-341B76BEA0F4':False,'0BFC35E2-4817-4865-BFA7-764742302A2D':False,'A76A5AF5-5A93-4CF2-A16E-62353BB70E8A':True, '4FC32141-E888-4BFF-8804-12559A491D8C':True, 'B09E373F-8A54-44C8-895B-0039390B859F':True}


ts_users = ['user_0','user_1','user_2','user_3','user_4','user_5','user_6']

all_uuids = ['59EEFAE0-DEB0-4FFF-9250-54D2A03D0CF2', '7CE37510-56D0-4120-A1CF-0E23351428D2', 'ECECC2AB-D32F-4F90-B74C-E12A1C69BBE2', '0A986513-7828-4D53-AA1F-E02D6DF9561B', '481F4DD2-7689-43B9-A2AA-C8772227162B', '806289BC-AD52-4CC1-806C-0CDB14D65EB6', '136562B6-95B2-483D-88DC-065F28409FD2', '86A4F379-B305-473D-9D83-FC7D800180EF', '665514DE-49DC-421F-8DCB-145D0B2609AD', '3600D531-0C55-44A7-AE95-A7A38519464E', 'BEF6C611-50DA-4971-A040-87FB979F3FC1', 'A7599A50-24AE-46A6-8EA6-2576F1011D81', 'CF722AA9-2533-4E51-9FEB-9EAC84EE9AAC', '83CF687B-7CEC-434B-9FE8-00C3D5799BE6', '00EABED2-271D-49D8-B599-1D4A09240601', 'CCAF77F0-FABB-4F2F-9E24-D56AD0C5A82F', '7D9BB102-A612-4E2A-8E22-3159752F55D8', 'C48CE857-A0DD-4DDB-BEA5-3A25449B2153', '4E98F91F-4654-42EF-B908-A3389443F2E7', '96A358A0-FFF2-4239-B93E-C7425B901B47', 'FDAA70A1-42A3-4E3F-9AE3-3FDA412E03BF', '40E170A7-607B-4578-AF04-F021C3B0384A', 'CA820D43-E5E2-42EF-9798-BE56F776370B', 'B7F9D634-263E-4A97-87F9-6FFB4DDCB36C', '27E04243-B138-4F40-A164-F40B60165CF3', '2C32C23E-E30C-498A-8DD2-0EFB9150A02E', 'A5CDF89D-02A2-4EC1-89F8-F534FDABDD96', 'B9724848-C7E2-45F4-9B3F-A1F38D864495', '11B5EC4D-4133-4289-B475-4E737182A406', '1155FF54-63D3-4AB2-9863-8385D0BD0A13', 'CDA3BBF7-6631-45E8-85BA-EEB416B32A3C', '61976C24-1C50-4355-9C49-AAE44A7D09F6', '0E6184E1-90C0-48EE-B25A-F1ECB7B9714E', 'A5A30F76-581E-4757-97A2-957553A2C6AA', '1538C99F-BA1E-4EFB-A949-6C7C47701B20', '5EF64122-B513-46AE-BCF1-E62AAC285D2C','74B86067-5D4B-43CF-82CF-341B76BEA0F4','59818CD2-24D7-4D32-B133-24C2FE3801E5','B09E373F-8A54-44C8-895B-0039390B859F','D7D20E2E-FC78-405D-B346-DBD3FD8FC92B','9DC38D04-E82E-4F29-AB52-B476535226F2','F50235E0-DD67-4F2A-B00B-1F31ADA998B9','0BFC35E2-4817-4865-BFA7-764742302A2D','4FC32141-E888-4BFF-8804-12559A491D8C','A76A5AF5-5A93-4CF2-A16E-62353BB70E8A','098A72A5-E3E5-4F54-A152-BBDA0DF7B694','797D145F-3858-4A7F-A7C2-A4EB721E133C','99B204C0-DD5C-4BB7-83E8-A37281B8D769','1DBB0F6F-1F81-4A50-9DF4-CD62ACFA4842','24E40C4C-A349-4F9F-93AB-01D00FB994AF','33A85C34-CFE4-4732-9E73-0A7AC861B27A','8023FE1A-D3B0-4E2C-A57A-9321B7FC755F','5152A2DF-FAF3-4BA8-9CA9-E66B32671A53','78A91A4E-4A51-4065-BDA7-94755F0BB3BB','5119D0F8-FCA8-4184-A4EB-19421A40DE0D','81536B0A-8DBF-4D8A-AC24-9543E2E4C8E0','9759096F-1119-4E19-A0AD-6F16989C7E1C','61359772-D8D8-480D-B623-7C636EAD0C81','E65577C1-8D5D-4F70-AF23-B3ADB9D3DBA3','BE3CA5A6-A561-4BBD-B7C9-5DF6805400FC']
normalized = {'user_0':False,'user_1':False,'user_2':False,'user_3':False,'user_4':False,'user_5':False,'user_6':False,'E65577C1-8D5D-4F70-AF23-B3ADB9D3DBA3':True,'BE3CA5A6-A561-4BBD-B7C9-5DF6805400FC':True,'C48CE857-A0DD-4DDB-BEA5-3A25449B2153':True,'61359772-D8D8-480D-B623-7C636EAD0C81':False,'9759096F-1119-4E19-A0AD-6F16989C7E1C':True,'665514DE-49DC-421F-8DCB-145D0B2609AD':False,'81536B0A-8DBF-4D8A-AC24-9543E2E4C8E0':False,'8023FE1A-D3B0-4E2C-A57A-9321B7FC755F':True,'5152A2DF-FAF3-4BA8-9CA9-E66B32671A53':True,'5119D0F8-FCA8-4184-A4EB-19421A40DE0D':True,'78A91A4E-4A51-4065-BDA7-94755F0BB3BB':True,'33A85C34-CFE4-4732-9E73-0A7AC861B27A':False,'24E40C4C-A349-4F9F-93AB-01D00FB994AF':True,'1DBB0F6F-1F81-4A50-9DF4-CD62ACFA4842':False,'0A986513-7828-4D53-AA1F-E02D6DF9561B':True,'797D145F-3858-4A7F-A7C2-A4EB721E133C':False,'99B204C0-DD5C-4BB7-83E8-A37281B8D769':False,'098A72A5-E3E5-4F54-A152-BBDA0DF7B694':True,'74B86067-5D4B-43CF-82CF-341B76BEA0F4':False,'4FC32141-E888-4BFF-8804-12559A491D8C':True,'59818CD2-24D7-4D32-B133-24C2FE3801E5':False, 'D7D20E2E-FC78-405D-B346-DBD3FD8FC92B':True,'F50235E0-DD67-4F2A-B00B-1F31ADA998B9':False,'9DC38D04-E82E-4F29-AB52-B476535226F2':True,'99B204C0-DD5C-4BB7-83E8-A37281B8D769':False, '4FC32141-E888-4BFF-8804-12559A491D8C':True,'098A72A5-E3E5-4F54-A152-BBDA0DF7B694':True,'74B86067-5D4B-43CF-82CF-341B76BEA0F4':False,'0BFC35E2-4817-4865-BFA7-764742302A2D':False,'A76A5AF5-5A93-4CF2-A16E-62353BB70E8A':True, '4FC32141-E888-4BFF-8804-12559A491D8C':True, 'B09E373F-8A54-44C8-895B-0039390B859F':True, '59EEFAE0-DEB0-4FFF-9250-54D2A03D0CF2': False, '7CE37510-56D0-4120-A1CF-0E23351428D2': True, 'ECECC2AB-D32F-4F90-B74C-E12A1C69BBE2': True, '0A986513-7828-4D53-AA1F-E02D6DF9561B': True, '481F4DD2-7689-43B9-A2AA-C8772227162B': False, '806289BC-AD52-4CC1-806C-0CDB14D65EB6': False, '136562B6-95B2-483D-88DC-065F28409FD2': True, '86A4F379-B305-473D-9D83-FC7D800180EF': False, '665514DE-49DC-421F-8DCB-145D0B2609AD': False, '3600D531-0C55-44A7-AE95-A7A38519464E': True, 'BEF6C611-50DA-4971-A040-87FB979F3FC1': False, 'A7599A50-24AE-46A6-8EA6-2576F1011D81': False, 'CF722AA9-2533-4E51-9FEB-9EAC84EE9AAC': True, '83CF687B-7CEC-434B-9FE8-00C3D5799BE6': True, '00EABED2-271D-49D8-B599-1D4A09240601': True, 'CCAF77F0-FABB-4F2F-9E24-D56AD0C5A82F': False, '7D9BB102-A612-4E2A-8E22-3159752F55D8': True, 'C48CE857-A0DD-4DDB-BEA5-3A25449B2153': True, '4E98F91F-4654-42EF-B908-A3389443F2E7': False, '96A358A0-FFF2-4239-B93E-C7425B901B47': False, 'FDAA70A1-42A3-4E3F-9AE3-3FDA412E03BF': True, '40E170A7-607B-4578-AF04-F021C3B0384A': False, 'CA820D43-E5E2-42EF-9798-BE56F776370B': True, 'B7F9D634-263E-4A97-87F9-6FFB4DDCB36C': False, '27E04243-B138-4F40-A164-F40B60165CF3': False, '2C32C23E-E30C-498A-8DD2-0EFB9150A02E': True, 'A5CDF89D-02A2-4EC1-89F8-F534FDABDD96': True, 'B9724848-C7E2-45F4-9B3F-A1F38D864495': True, '11B5EC4D-4133-4289-B475-4E737182A406': True, '1155FF54-63D3-4AB2-9863-8385D0BD0A13': False, 'CDA3BBF7-6631-45E8-85BA-EEB416B32A3C': True, '61976C24-1C50-4355-9C49-AAE44A7D09F6': True, '0E6184E1-90C0-48EE-B25A-F1ECB7B9714E': False, 'A5A30F76-581E-4757-97A2-957553A2C6AA': False, '1538C99F-BA1E-4EFB-A949-6C7C47701B20': True, '5EF64122-B513-46AE-BCF1-E62AAC285D2C': True }



#headers
features_header_sel = ['mMean', 'mVar', 'mPtP', 'mMax', 'mMin', 'mSkew', 'mKurt', 'mEner', 'xMean', 'yMean', 'zMean','xVar', 'yVar', 'zVar', 'xPtP', 'yPtP', 'zPtP','xy_crossc','yz_crossc','xz_crossc']
all_header =   ['mMean', 'mVar', 'mPtP', 'mMax', 'mMin', 'mSkew', 'mKurt', 'mEner', 'xMean', 'yMean', 'zMean','xVar', 'yVar', 'zVar', 'xPtP', 'yPtP', 'zPtP','xy_crossc','yz_crossc','xz_crossc','class']
phheader = ['uuid','wal_mean','wal_std','run_mean','run_std','cyc_mean','cyc_std']
gtheader = ['label','ts','steps']

new_features_set = ['mMean', 'mVar', 'mPtP', 'mMax', 'mMin', 'mSkew', 'mKurt', 'mEner', 'xMean', 'yMean', 'zMean','xVar', 'yVar', 'zVar', 'xPtP', 'yPtP', 'zPtP','xy_crossc','yz_crossc','xz_crossc','fNumPeaks','fNumPos']
new_features_header = ['ts','mMean', 'mVar', 'mPtP', 'mMax', 'mMin', 'mSkew', 'mKurt', 'mEner', 'xMean', 'yMean', 'zMean','xVar', 'yVar', 'zVar', 'xPtP', 'yPtP', 'zPtP','xy_crossc','yz_crossc','xz_crossc','fNumPeaks','fNumPos']
new_features_header_class = ['ts','mMean', 'mVar', 'mPtP', 'mMax', 'mMin', 'mSkew', 'mKurt', 'mEner', 'xMean', 'yMean', 'zMean','xVar', 'yVar', 'zVar', 'xPtP', 'yPtP', 'zPtP','xy_crossc','yz_crossc','xz_crossc','fNumPeaks','fNumPos','label']
#wgt
#SEDENTARY,0.9765121804277656,1445384906,1445385203,00:48:26,00:53:23
wgtheader = ['weak','prob','ts1','ts2','hh1','hh2','speed']

#paths
myfeaturespath = '/home/fedecrux/python/data/Extrasensory/myfeatures/'
loopath = '/home/fedecrux/python/data/Extrasensory/leave-one-out/'
rawdatafolder = '/home/fedecrux/python/data/Extrasensory/raw_data/'
gyrodatafolder = '/home/fedecrux/raw_gyro/proc_gyro/'
watchdatafolder = '/home/fedecrux/watch/watch_acc/'
gtfolder = '/home/fedecrux/python/data/Extrasensory/gt/'
featureslabelsfolder = '/home/fedecrux/python/data/Extrasensory/features_labels/'
#import peakutils
#import StringIO

def save_classifier(classifier, name):
   f = open(name+'.pickle', 'wb')
   pickle.dump(classifier, f, -1)
   f.close()

def load_classifier(name):
   f = open(name+'.pickle', 'rb')
   classifier = pickle.load(f)
   f.close()
   return classifier

def load_auto_train(path):
	f = open(path+'train.AUTO.X.pickle','rb')
	X_validation = pickle.load(f)
	f.close()

	f = open(path+'train.AUTO.y.pickle','rb')
	y_validation = pickle.load(f)
	f.close()
	return X_validation,y_validation

def load_validation(iteration):
	f = open(iteration+'validation.X.pickle','rb')
	X_validation = pickle.load(f)
	f.close()

	f = open(iteration+'validation.y.pickle','rb')
	y_validation = pickle.load(f)
	f.close()
	return X_validation,y_validation

def load_random_train_test(iteration):
	f = open(iteration+'.train.random.X.pickle','rb')
	X_train = pickle.load(f)
	f.close()

	f = open(iteration+'.train.random.y.pickle','rb')
	y_train = pickle.load(f)
	f.close()


	f = open(iteration+'.test.random.X.pickle','rb')
	X_test = pickle.load(f)
	f.close()

	#f = open('y.pickle.backup_no_overlap', 'rb')
	f = open(iteration+'.test.random.y.pickle','rb')
	y_test = pickle.load(f)
	f.close()
	return X_train,y_train,X_test,y_test

def load_train_test(iteration):
	f = open(iteration+'.train.X.pickle','rb')
	X_train = pickle.load(f)
	f.close()

	f = open(iteration+'.train.y.pickle','rb')
	y_train = pickle.load(f)
	f.close()

	f = open(iteration+'.train.y.fully.pickle','rb')
	y_train_fully = pickle.load(f)
	f.close()

	f = open(iteration+'.test.X.pickle','rb')
	X_test = pickle.load(f)
	f.close()

	#f = open('y.pickle.backup_no_overlap', 'rb')
	f = open(iteration+'.test.y.pickle','rb')
	y_test = pickle.load(f)
	f.close()
	return X_train,y_train,y_train_fully,X_test,y_test


#def convertToDeepSamples(uuid,timestamp,filename,label,weak_label)
def deleteSamples(uuid,traintest,output_folder = "/home/fedecrux/python/data/Extrasensory/deep_samples/"):
	if os.path.exists(output_folder+uuid+"/"+traintest+".ACC_X.csv"):
		os.remove(output_folder+uuid+"/"+traintest+".ACC_X.csv")
	if os.path.exists(output_folder+uuid+"/"+traintest+".ACC_Y.csv"):
		os.remove(output_folder+uuid+"/"+traintest+".ACC_Y.csv")
	if os.path.exists(output_folder+uuid+"/"+traintest+".ACC_Z.csv"):
		os.remove(output_folder+uuid+"/"+traintest+".ACC_Z.csv")
	if os.path.exists(output_folder+uuid+"/"+traintest+".y_LABEL.csv"):
		os.remove(output_folder+uuid+"/"+traintest+".y_LABEL.csv")
	if os.path.exists(output_folder+uuid+"/"+traintest+".y_WEAK.csv"):
		os.remove(output_folder+uuid+"/"+traintest+".y_WEAK.csv")
	#os.remove(output_folder+uuid+"/"+traintest+".y_LABEL.csv")

#WARNING: this function returns only time domain features
def computeFeaturesOnWindow(mag,x,y,z):
		features = []
		mag_mean = np.mean(mag) #f1
		mag_var = np.var(mag) #f2
		mag_ptp = np.ptp(mag) #f3
		mag_max = np.max(mag) #f4
		mag_min = np.min(mag) #f5
		mag_skew = skew(mag) #f6
		mag_kurt = kurtosis(mag) #f7
		#channels
		x_mean = abs(np.mean(x)) #f8
		y_mean = abs(np.mean(y)) #f9
		z_mean = abs(np.mean(z)) #f10
		x_var = abs(np.var(x)) #f11
		y_var = abs(np.var(y)) #f12
		z_var = abs(np.var(z)) #f13
		x_ptp = abs(np.ptp(x)) #f14
		y_ptp = abs(np.ptp(y)) #f15
		z_ptp = abs(np.ptp(z)) #f16
		#cross-correlation between channels
		#print("x: ",x)
		#print("y: ",y)
		xy_cross = np.corrcoef(x,y)[0,1] #f17
		yz_cross = np.corrcoef(y,z)[0,1] #f18
		xz_cross = np.corrcoef(x,z)[0,1] #f19
		if np.isnan(xy_cross):
			xy_cross = 0.
			print("NaN: 0.")
			#print("x: ",x)
			#print("y: ",y)
		if np.isnan(xz_cross):
			xz_cross = 0.
			#print("NaN: 0.")
		if np.isnan(yz_cross):
			yz_cross = 0.
			#print("NaN: 0.")
		#print('NEW:', xy_cross,' - ',yz_cross,' - ',xz_cross )
		#freq = scipy.fft(mag)
		#computing DSP iwth FFT
		###### Using Frequency domain featires and cross correlations
		freq = abs(scipy.fft(mag))
		N = len(mag)
		#energy of the SPD
		# the discrete verion of Parseval theorem
		energy_fft = np.sum(np.abs(freq)**2)/N  #f20

		#energy_t = np.sum(mag**2)

		#print(energy_t, " time - fft", energy_fft)
		'''peak_widths = np.arange(2,5)
		peaks = signal.find_peaks_cwt(freq, peak_widths)
		peaks_indexes = detect_peaks.detect_peaks(signal,mph=1.5,mpd=10)
		return len(indexes)*3
		#print(peaks)
		fNumPeaks = len(peaks)-1 #f21
		filtered_peaks = []
		filtered_indexes = []
		for peak in peaks:
			max = 0.
			if peak != 1:
				#print("value: ",freq[peak-1])
				filtered_peaks.append(freq[peak-1])
				filtered_indexes.append(peak-1)
		#print(filtered_indexes)
		#print(filtered_peaks)
		fpeaks = np.array(filtered_peaks)
		if len(filtered_peaks) > 1:
			max_index = fpeaks.argmax(axis=0)
		#print(filtered_indexes[max] , " -> ",filtered_peaks[max] )
			fMaxPos = filtered_indexes[max_index] #f22
		else:
			fMaxPos = 0

		if math.isnan(fMaxPos) or math.isnan(fNumPeaks):
			print("NaN value in feature set")'''

		peaks_indexes = detect_peaks.detect_peaks(freq,mph=0.1,mpd=3)
		fNumPeaks = len(peaks_indexes)
		#print(peaks_indexes)
		#if fNumPeaks > 1:
		#	fMaxPos = peaks_indexes[1]
		if fNumPeaks > 0:
			fMaxPos = peaks_indexes[0]
		else:
			fMaxPos = 0

		#print( "Freq: ", fNumPeaks, " - ", fMaxPos  )

		features.append(mag_mean)
		features.append(mag_var)
		features.append(mag_ptp)
		features.append(mag_max)
		features.append(mag_min)
		features.append(mag_skew)
		features.append(mag_kurt)
		features.append(energy_fft)
		features.append(x_mean)
		features.append(y_mean)
		features.append(z_mean)
		features.append(x_var)
		features.append(y_var)
		features.append(z_var)
		features.append(x_ptp)
		features.append(y_ptp)
		features.append(z_ptp)
		features.append(abs(xy_cross))
		features.append(abs(yz_cross))
		features.append(abs(xz_cross))
		features.append(float(fNumPeaks))
		features.append(float(fMaxPos))

		return features

def computeStepCounts(uuids):
	for uuid in uuids:
		if os.path.exists(gtfolder+uuid+"_steps.csv"):
			os.remove(gtfolder+uuid+"_steps.csv")
		print("Computing spm for user: ",uuid)
		rawfiles = glob.glob(rawdatafolder+uuid+"/*",recursive=False)
		with open(gtfolder+uuid+"_steps.csv", 'a+') as steps_file:
			for idx,rawfile in enumerate(rawfiles):
				printProgressBar(idx, len(rawfiles), prefix = 'Progress:', suffix = 'Complete', length = 50)
				spm = findPeaksFile(rawfile,normalized[uuid])
				ts = rawfile.replace(".m_raw_acc.dat.gz","")
				ts = ts.replace(rawdatafolder+uuid+"/","")
				steps_file.write( ts + "," + str(spm) + "\n" )
		print("DONE: ",uuid)

def computeAllFeaturesForUser(uuid,normalized):
	print("Computing Features: ",uuid)
	if os.path.exists(myfeaturespath+uuid+"/all_features.csv"):
			os.remove(myfeaturespath+uuid+"/all_features.csv")

	rawfiles = glob.glob(rawdatafolder+uuid+"/*",recursive=False)
	with open(myfeaturespath+uuid+"/all_features.csv", 'a+') as feats_file:
		for idx,rawfile in enumerate(rawfiles):
			timestampfromfile = rawfile.split(rawdatafolder+uuid+"/")[1]
			timestampstring = timestampfromfile.split(".m_raw_acc.dat.gz")[0]
			ts = int(timestampstring)
			printProgressBar(idx, len(rawfiles), prefix = 'Progress:', suffix = 'Complete', length = 50)
			raw_data = pd.read_csv(rawfile,names=['ts','x','y','z'],sep=' ', compression='gzip')
			num_samples = len(raw_data)
			#print("Features: ",uuid," : ", timestamp, " normalized: ", normalized)
			mags = []
			for irow,rawrow in raw_data.iterrows():
				gravity = 9.80665
				sample = [rawrow['x'],rawrow['y'],rawrow['z']]
				if normalized == False:
					sample = [rawrow['x']/gravity,rawrow['y']/gravity,rawrow['z']/gravity]
				mag = np.linalg.norm(sample)
				mags.append(mag)

			if normalized == True:
				df_x = raw_data[['x']]
				data_x = df_x['x'].tolist()
				df_y = raw_data[['y']]
				data_y = df_y['y'].tolist()
				df_z = raw_data[['z']]
				data_z = df_z['z'].tolist()
			else:
				df_x = raw_data[['x']] / gravity
				data_x = df_x['x'].tolist()
				df_y = raw_data[['y']] / gravity
				data_y = df_y['y'].tolist()
				df_z = raw_data[['z']] / gravity
				data_z = df_z['z'].tolist()
			start = 0
			end = start + 160
			features = []
			while end < num_samples:
				window_x = data_x[start:end]
				window_y = data_y[start:end]
				window_z = data_z[start:end]
				window_mag = mags[start:end]
				#print(start, " - ", end," ", len(window_x))
				ft = computeFeaturesOnWindow(window_mag,window_x,window_y,window_z)
				feats_file.write( str(ts) + "," )
				for i,f in enumerate(ft):
					feats_file.write(str(f))
					if i < (len(ft)-1):
						feats_file.write(",")
					else:
						feats_file.write("\n")
				ts += 1
				start += 40
				end += 40	#spm = extrautils.findPeaksFile(rawfile,normalized[uuid])
						#ts = rawfile.replace(".m_raw_acc.dat.gz","")
						#ts = ts.replace(rawdatafolder+uuid+"/","")
	print("DONE: ",uuid)

#NOTE New function ACC+GYRO
def extractAccGyroFeatures(uuid,timestamp,normalized,rnd=0, step=120,accdatapath='',gyrodatapath=''):
	if accdatapath == '':
		accdatapath = rawdatafolder
	if gyrodatapath == '':
		gyrodatapath = gyrodatafolder
	rawfile = accdatapath+uuid+'/'+str(timestamp)+'.m_raw_acc.dat.gz'
	#'m_watch_acc.dat.gz'
	gyrofile = gyrodatapath+uuid+'/'+str(timestamp)+'.m_proc_gyro.dat.gz'
	if not os.path.exists(str(rawfile)):
			print(rawfile,": not found.")
			return []
	if not os.path.exists(str(gyrofile)):
			print(gyrofile,": not found.")
			return []
	raw_data = pd.read_csv(rawfile,names=['ts','x','y','z'],sep='\s|,',engine="python", compression='gzip')
	raw_gyro = pd.read_csv(gyrofile,names=['ts','x','y','z'],sep='\s|,',engine="python", compression='gzip')
	num_samples = min(len(raw_data),len(raw_gyro))
	#print(raw_gyro.head())
	#print("Features: ",uuid," : ", timestamp, " normalized: ", normalized)
	mags = []
	for irow,rawrow in raw_data.iterrows():
		gravity = 9.80665
		sample = [rawrow['x'],rawrow['y'],rawrow['z']]
		if normalized == False:
			sample = [rawrow['x']/gravity,rawrow['y']/gravity,rawrow['z']/gravity]
		mag = np.linalg.norm(sample)
		mags.append(mag)

	mags_gyro = []
	for irow,rawrow in raw_gyro.iterrows():
		sample = [rawrow['x'],rawrow['y'],rawrow['z']]
		mag = np.linalg.norm(sample)
		mags_gyro.append(mag)

	if normalized == True:
		df_x = raw_data[['x']]
		data_x = df_x['x'].tolist()
		df_y = raw_data[['y']]
		data_y = df_y['y'].tolist()
		df_z = raw_data[['z']]
		data_z = df_z['z'].tolist()
	else:
		df_x = raw_data[['x']] / gravity
		data_x = df_x['x'].tolist()
		df_y = raw_data[['y']] / gravity
		data_y = df_y['y'].tolist()
		df_z = raw_data[['z']] / gravity
		data_z = df_z['z'].tolist()

	#GYRO DATA
	df_x_gyro = raw_gyro[['x']]
	data_x_gyro = df_x_gyro['x'].tolist()
	df_y_gyro = raw_gyro[['y']]
	data_y_gyro = df_y_gyro['y'].tolist()
	df_z_gyro = raw_gyro[['z']]
	data_z_gyro = df_z_gyro['z'].tolist()
	#with open(myfeaturesfolder+uuid+"/"+train_test+".GT.csv", "a") as feats_file:
	start = 0 + rnd
	end = start + 160
	features = []
	while end < num_samples:
		window_x = data_x[start:end]
		window_y = data_y[start:end]
		window_z = data_z[start:end]
		window_mag = mags[start:end]
		ft = computeFeaturesOnWindow(window_mag,window_x,window_y,window_z)
		gyro_x = data_x_gyro[start:end]
		gyro_y = data_y_gyro[start:end]
		gyro_z = data_z_gyro[start:end]
		gyro_mag = mags_gyro[start:end]
		ft_gyro = computeFeaturesOnWindow(gyro_mag,gyro_x,gyro_y,gyro_z)
		ft.extend(ft_gyro)
		#print(ft)
		#print("mean: ",ft[0])
		features.append(ft)
		start += step
		end += step
	return features

#######Filter body acc components
def butter_lowpass(cutoff, fs, order=1):
    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    b, a = signal.butter(order, normal_cutoff, btype='low', analog=False)
    return b, a

def butter_lowpass_filter(data, cutoff, fs, order=1):
    b, a = butter_lowpass(cutoff, fs, order=order)
    y = signal.filtfilt(b, a, data)
    return y


def extractBodyAccGyroFeatures(uuid,timestamp,normalized,rnd=0, step=120):
	rawfile = rawdatafolder+uuid+'/'+str(timestamp)+'.m_raw_acc.dat.gz'
	#'m_watch_acc.dat.gz'
	gyrofile = gyrodatafolder+uuid+'/'+str(timestamp)+'.m_proc_gyro.dat.gz'
	if not os.path.exists(str(rawfile)):
			print(rawfile,": not found.")
			return []
	if not os.path.exists(str(gyrofile)):
			print(gyrofile,": not found.")
			return []
	raw_data = pd.read_csv(rawfile,names=['ts','x','y','z'],sep=' ', compression='gzip')
	####
	raw_acc = raw_data[['x','y','z']]
	gravity = 9.80665
	if normalized == True:
		df_x = raw_acc[['x']]
		data_x = df_x['x'].tolist()
		df_y = raw_acc[['y']]
		data_y = df_y['y'].tolist()
		df_z = raw_acc[['z']]
		data_z = df_z['z'].tolist()
	else:
		df_x = raw_acc[['x']] / gravity
		data_x = df_x['x'].tolist()
		df_y = raw_acc[['y']] / gravity
		data_y = df_y['y'].tolist()
		df_z = raw_acc[['z']] / gravity
		data_z = df_z['z'].tolist()

	
	order = 2
	fs = 40.0       # sample rate, Hz
	cutoff = 0.3 #0.3 Hz desired cutoff frequency of the filter, Hz

	grav_data_x = butter_lowpass_filter(np.array(data_x), cutoff, fs, order)
	grav_data_y = butter_lowpass_filter(np.array(data_y), cutoff, fs, order)
	grav_data_z = butter_lowpass_filter(np.array(data_z), cutoff, fs, order)
	###########
	raw_gyro = pd.read_csv(gyrofile,names=['ts','x','y','z'],sep=' ', compression='gzip')
	num_samples = min(len(raw_data),len(raw_gyro))
	#print(raw_gyro.head())
	#print("Features: ",uuid," : ", timestamp, " normalized: ", normalized)
	mags = []
	for irow,rawrow in raw_data.iterrows():
		gravity = 9.80665
		sample = [rawrow['x'],rawrow['y'],rawrow['z']]
		if normalized == False:
			sample = [rawrow['x']/gravity,rawrow['y']/gravity,rawrow['z']/gravity]
		mag = np.linalg.norm(sample)
		mags.append(mag)

	grav_data_mag = butter_lowpass_filter(np.array(mags), cutoff, fs, order)

	mags_gyro = []
	for irow,rawrow in raw_gyro.iterrows():
		sample = [rawrow['x'],rawrow['y'],rawrow['z']]
		mag = np.linalg.norm(sample)
		mags_gyro.append(mag)


	#GYRO DATA
	df_x_gyro = raw_gyro[['x']]
	data_x_gyro = df_x_gyro['x'].tolist()
	df_y_gyro = raw_gyro[['y']]
	data_y_gyro = df_y_gyro['y'].tolist()
	df_z_gyro = raw_gyro[['z']]
	data_z_gyro = df_z_gyro['z'].tolist()
	#with open(myfeaturesfolder+uuid+"/"+train_test+".GT.csv", "a") as feats_file:
	start = 0 + rnd
	end = start + 160
	features = []
	while end < num_samples:
		window_x = data_x[start:end]
		window_y = data_y[start:end]
		window_z = data_z[start:end]
		window_mag = mags[start:end]
		ft = computeFeaturesOnWindow(window_mag,window_x,window_y,window_z)
		grav_x = grav_data_x[start:end]
		grav_y = grav_data_y[start:end]
		grav_z = grav_data_z[start:end]
		grav_mag = grav_data_mag[start:end]
		ft_grav = computeFeaturesOnWindow(grav_mag,grav_x,grav_y,grav_z)
		ft.extend(ft_grav)
		gyro_x = data_x_gyro[start:end]
		gyro_y = data_y_gyro[start:end]
		gyro_z = data_z_gyro[start:end]
		gyro_mag = mags_gyro[start:end]
		ft_gyro = computeFeaturesOnWindow(gyro_mag,gyro_x,gyro_y,gyro_z)
		ft.extend(ft_gyro)
		#print(ft)
		#print("mean: ",ft[0])
		features.append(ft)
		start += step
		end += step
	return features


def extractWatchFeatures(uuid,timestamp,normalized,rnd=0,step=120):
	rawfile = watchdatafolder+uuid+'/'+str(timestamp)+'.m_watch_acc.dat.gz'
	if not os.path.exists(str(rawfile)):
			print(rawfile,": not found.")
			return []
	raw_data = pd.read_csv(rawfile,names=['ts','x','y','z'],sep=' ', compression='gzip')
	num_samples = len(raw_data)
	#print("Features: ",uuid," : ", timestamp, " normalized: ", normalized)
	mags = []
	for irow,rawrow in raw_data.iterrows():
		gravity = 9.80665
		sample = [rawrow['x'],rawrow['y'],rawrow['z']]

		#if irow == 0 and normalized == False:
		#	print("Normalizing [m/s^2]->[g]: ", sample)
		if normalized == False:
			sample = [rawrow['x']/gravity,rawrow['y']/gravity,rawrow['z']/gravity]
		#if irow == 0 and normalized == False:
		#	print("Normalized: ", sample)
		mag = np.linalg.norm(sample)
		mags.append(mag)

	if normalized == True:
		df_x = raw_data[['x']]
		data_x = df_x['x'].tolist()
		df_y = raw_data[['y']]
		data_y = df_y['y'].tolist()
		df_z = raw_data[['z']]
		data_z = df_z['z'].tolist()
	else:
		df_x = raw_data[['x']] / gravity
		data_x = df_x['x'].tolist()
		df_y = raw_data[['y']] / gravity
		data_y = df_y['y'].tolist()
		df_z = raw_data[['z']] / gravity
		data_z = df_z['z'].tolist()

	#with open(myfeaturesfolder+uuid+"/"+train_test+".GT.csv", "a") as feats_file:
	start = 0 + rnd
	end = start + 160
	features = []
	while end < num_samples:
		window_x = data_x[start:end]
		window_y = data_y[start:end]
		window_z = data_z[start:end]
		window_mag = mags[start:end]
		#print(start, " - ", end," ", len(window_x))
		ft = computeFeaturesOnWindow(window_mag,window_x,window_y,window_z)
		#print(ft)
		#print("mean: ",ft[0])
		features.append(ft)
		start += step
		end += step
	return features

#NOTE: this functions is used in COMPARE GT WGT to generate automatic training dataset
def extractFeatures(uuid,timestamp,normalized,rnd=0):
	rawfile = rawdatafolder+uuid+'/'+str(timestamp)+'.m_raw_acc.dat.gz'
	if not os.path.exists(str(rawfile)):
			print(rawfile,": not found.")
			return []
	raw_data = pd.read_csv(rawfile,names=['ts','x','y','z'],sep=' ', compression='gzip')
	num_samples = len(raw_data)
	#print("Features: ",uuid," : ", timestamp, " normalized: ", normalized)
	mags = []
	for irow,rawrow in raw_data.iterrows():
		gravity = 9.80665
		sample = [rawrow['x'],rawrow['y'],rawrow['z']]

		#if irow == 0 and normalized == False:
		#	print("Normalizing [m/s^2]->[g]: ", sample)
		if normalized == False:
			sample = [rawrow['x']/gravity,rawrow['y']/gravity,rawrow['z']/gravity]
		#if irow == 0 and normalized == False:
		#	print("Normalized: ", sample)
		mag = np.linalg.norm(sample)
		mags.append(mag)

	if normalized == True:
		df_x = raw_data[['x']]
		data_x = df_x['x'].tolist()
		df_y = raw_data[['y']]
		data_y = df_y['y'].tolist()
		df_z = raw_data[['z']]
		data_z = df_z['z'].tolist()
	else:
		df_x = raw_data[['x']] / gravity
		data_x = df_x['x'].tolist()
		df_y = raw_data[['y']] / gravity
		data_y = df_y['y'].tolist()
		df_z = raw_data[['z']] / gravity
		data_z = df_z['z'].tolist()

	#with open(myfeaturesfolder+uuid+"/"+train_test+".GT.csv", "a") as feats_file:
	start = 0 + rnd
	end = start + 160
	features = []
	while end < num_samples:
		window_x = data_x[start:end]
		window_y = data_y[start:end]
		window_z = data_z[start:end]
		window_mag = mags[start:end]
		#print(start, " - ", end," ", len(window_x))
		ft = computeFeaturesOnWindow(window_mag,window_x,window_y,window_z)
		#print(ft)
		#print("mean: ",ft[0])
		features.append(ft)
		start += 80
		end += 80
	return features




def extractSamples(uuid,timestamp,traintest,label,weak_label,normalized,output_folder = "/home/fedecrux/python/data/Extrasensory/deep_samples/"):
	rawdatafolder = "/home/fedecrux/python/data/Extrasensory/raw_data/"
	rawfile = rawdatafolder+uuid+'/'+str(timestamp)+'.m_raw_acc.dat.gz'
	if not os.path.exists(str(rawfile)):
			print(rawfile,": not found.")
			return
	raw_data = pd.read_csv(rawfile,names=['ts','x','y','z'],sep=' ', compression='gzip')
	num_samples = len(raw_data)
	#print("Samples: ", uuid," ",timestamp, " ", num_samples," samples")
	#print(rawfile," : ",num_samples," samples")

	gravity = 9.80665
	conversion = 1.
	if normalized == False:
		conversion = gravity
	#print(conversion, " ",normalized)
	df_x = raw_data[['x']] / conversion
	data_x = df_x['x'].tolist()
	df_y = raw_data[['y']] /conversion
	data_y = df_y['y'].tolist()
	df_z = raw_data[['z']] / conversion
	data_z = df_z['z'].tolist()
	with open(output_folder+uuid+"/"+traintest+".y_LABEL.csv", 'a+') as y_label_all, open(output_folder+uuid+"/"+traintest+".y_WEAK.csv", 'a+') as y_weak_all, open(output_folder+uuid+"/"+traintest+".ACC_X.csv", 'a+') as x_file, open(output_folder+uuid+"/"+traintest+".ACC_Y.csv", 'a+') as y_file, open(output_folder+uuid+"/"+traintest+".ACC_Z.csv", 'a+') as z_file:
	#with open(myfeaturesfolder+uuid+".X.csv", "a") as x_file:
		start = 0
		end = start + 128
		while end < num_samples:
			window_x = data_x[start:end]
			window_y = data_y[start:end]
			window_z = data_z[start:end]
			#print(window_x[0]," ",window_y[0]," ",window_z[0])
			for item in range(len(window_x)):
				x_file.write(str(window_x[item]))
				y_file.write(str(window_y[item]))
				z_file.write(str(window_z[item]))
				if item < (len(window_x)-1):
					x_file.write(",")
					y_file.write(",")
					z_file.write(",")
			x_file.write("\n")
			y_file.write("\n")
			z_file.write("\n")
			y_weak_all.write(str(weak_label)+"\n")
			y_label_all.write(str(label)+"\n")
			start += 64
			end += 64


def plot_confusion_matrix(cm, classes,
						  normalize=False,
						  title='Confusion matrix'):
						  #cmap=plt.cm.Blues):
	"""
	This function prints and plots the confusion matrix.
	Normalization can be applied by setting `normalize=True`.
	"""
	if normalize:
		cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
		print("Normalized confusion matrix "+title)
	else:
		print('Confusion matrix, without normalization '+title)
	print(cm)

def getStepCountFromFile(uuid,timestamp):
	filename = "/home/fedecrux/python/data/Extrasensory/gt/"+uuid+"_steps.csv"
	steps_file = pd.read_csv(filename,names=['ts','steps'],sep=',')
	sel_ts = steps_file[ steps_file['ts'] == timestamp ]
	matches = sel_ts.values.tolist()
	if len(matches) > 1:
		print("WARNING: found more than one match")
	elif len(matches) == 0:
		#print("WARNING: no raw data for ",timestamp)
		return -1
	return int(matches[0][1])


def findPeaksFile(filename,normalized):
	raw_data = pd.read_csv(filename,names=['ts','x','y','z'],sep='\s|,', compression='gzip',engine='python')
	num_samples = len(raw_data)
	#print(num_samples)
	gravity = 9.80665
	conversion = 1.
	if normalized == False:
		conversion = gravity
	signal = []
	mag_mean = 0.
	for idx,row in raw_data.iterrows():
		sample = [ row['x']/conversion, row['y']/conversion, row['z']/conversion ]
		mag = np.linalg.norm(sample) #computing magnitude
		signal.append(mag)
		mag_mean += mag
	if len(signal) ==0:
		return 0
	mag_mean /= len(signal)
	#print(signal)
	#print(mag_mean)
	indexes = detect_peaks.detect_peaks(signal,mph=mag_mean+0.05,mpd=10)
	#input("pasue")
	return len(indexes)*3




def findPeaksTimestamp(uuid,timestamp,normalized):
	rawdatafolder = "/home/fedecrux/python/data/Extrasensory/raw_data/"
	rawfile = rawdatafolder+uuid+'/'+str(timestamp)+'.m_raw_acc.dat.gz'
	if not os.path.exists(str(rawfile)):
			#print(rawfile,": not found.")
			return -1
	raw_data = pd.read_csv(rawfile,names=['ts','x','y','z'],sep=' ', compression='gzip')
	num_samples = len(raw_data)
	#print("Samples: ", uuid," ",timestamp, " ", num_samples," samples")
	#print(rawfile," : ",num_samples," samples")


	gravity = 9.80665
	conversion = 1.
	if normalized == False:
		conversion = gravity

	signal = []
	for idx,row in raw_data.iterrows():
		sample = [ row['x']/conversion, row['y']/conversion, row['z']/conversion ]
		mag = np.linalg.norm(sample) #computing magnitude
		signal.append(mag)

	#print(signal)
	#expected 30-36
	print("WARNING: wrong thereshold")
	indexes = detect_peaks.detect_peaks(signal,mph=1.5,mpd=10)
	return len(indexes)*3

# Print iterations progress
def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█'):
	"""
	Call in a loop to create terminal progress bar
	@params:
		iteration   - Required  : current iteration (Int)
		total       - Required  : total iterations (Int)
		prefix      - Optional  : prefix string (Str)
		suffix      - Optional  : suffix string (Str)
		decimals    - Optional  : positive number of decimals in percent complete (Int)
		length      - Optional  : character length of bar (Int)
		fill        - Optional  : bar fill character (Str)
	"""
	percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
	filledLength = int(length * iteration // total)
	bar = fill * filledLength + '-' * (length - filledLength)
	print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end = '\r')
	# Print New Line on Complete
	if iteration == total:
		print()
