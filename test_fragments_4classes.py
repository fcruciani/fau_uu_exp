import numpy as np
import pandas as pd 
#from extrautils import normalized, rawdatafolder, gyrodatafolder
import extrautils
import ExpClassifiers as Classifiers
import os
from sklearn.metrics import classification_report, balanced_accuracy_score

#RS_Datazet users all android -> all in m/s^2 not normalized [g]
normalized = {'user_0':False,'user_1':False,'user_2':False,'user_3':False,'user_4':False,'user_5':False,'user_6':False}

rawdatafolder = "/home/fedecrux/python/data/uu_dataset/raw_acc/"
gyrodatafolder = "/home/fedecrux/python/data/uu_dataset/raw_gyro/"

ts_users = ['user_0','user_1','user_2','user_3','user_4','user_5','user_6']
#ts_users = ['user_0']#,'user_1']

def get_all_dataset(uuids):
	gt_df = pd.read_csv("GROUNDTRUTH_RSDATASET_ORIG.csv",names=['UUID','label','ts','class'])
	#exclude other uuids
	filtered_df = pd.DataFrame(columns=['UUID','label','ts','class'])
	for uuid in uuids:
		data_uuid = gt_df[ gt_df['UUID'] == uuid ]
		filtered_df = pd.concat([filtered_df,data_uuid], ignore_index=True)

	#print("all data: ",len(gt_df))
	#print("only uuids: ", len(filtered_df))
	df_sit_all = filtered_df[filtered_df['label'] == "SITTING" ] #0
	df_wal_all = filtered_df[filtered_df['label'] == "WALKING" ] #1
	df_run_all = filtered_df[filtered_df['label'] == "RUNNING" ] #2
	df_cyc_all = filtered_df[filtered_df['label'] == "CYCLING" ] #3
	#df_sup_all = filtered_df[filtered_df['label'] == "STAIRS_UP" ] #4
	#df_sdo_all = filtered_df[filtered_df['label'] == "STAIRS_DOWN" ] #5

	dataset_df = pd.concat([df_wal_all,df_sit_all,df_run_all,df_cyc_all], ignore_index=True)
	return dataset_df

def extractAccGyroSamples(uuid, ts, norm,step):
	X = []
	rawfile = rawdatafolder+uuid+"/"+ str(ts) +".m_raw_acc.dat.gz"
	gyrofile = gyrodatafolder+uuid+"/"+ str(ts) +".m_proc_gyro.dat.gz"
	raw_data_df = pd.read_csv(rawfile,names=['ts','ax','ay','az'],sep='\s|,', compression='gzip',engine='python')
	raw_data_df = raw_data_df[ ['ax','ay','az'] ]
	if normalized[uuid] == False:
		raw_data_df.loc[:,['ax']] = raw_data_df.loc[:,['ax']].div(9.80665)
		raw_data_df.loc[:,['ay']] = raw_data_df.loc[:,['ay']].div(9.80665)
		raw_data_df.loc[:,['az']] = raw_data_df.loc[:,['az']].div(9.80665)

	gyro_data_df = pd.read_csv(gyrofile,names=['ts','gx','gy','gz'],sep='\s|,', compression='gzip',engine='python')
	gyro_data_df = gyro_data_df[ ['gx','gy','gz'] ]
	'''gyro_stddev = np.zeros((1,3))
	gyro_stddev[:,0] = np.std(gyro_data_df['gx'].values)
	gyro_stddev[:,1] = np.std(gyro_data_df['gy'].values)
	gyro_stddev[:,2] = np.std(gyro_data_df['gz'].values)
	gyro_stddev[gyro_stddev == 0] = 1.
	gyro_mean = np.zeros((1,3))
	gyro_mean[:,0] = np.mean(gyro_data_df['gx'].values)
	gyro_mean[:,1] = np.mean(gyro_data_df['gy'].values)
	gyro_mean[:,2] = np.mean(gyro_data_df['gz'].values)'''
	start_index = 0
	end_index = 119
	while (end_index < len(raw_data_df)) and (end_index < len(gyro_data_df)):
		X_point = np.zeros((120,6))
		X_point[:,0] = np.reshape(raw_data_df.loc[start_index:end_index,['ax']].values, (120))
		X_point[:,1] = np.reshape(raw_data_df.loc[start_index:end_index,['ay']].values, (120))
		X_point[:,2] = np.reshape(raw_data_df.loc[start_index:end_index,['az']].values, (120))
		X_point[:,3] = np.reshape(gyro_data_df.loc[start_index:end_index,['gx']].values, (120))
		X_point[:,4] = np.reshape(gyro_data_df.loc[start_index:end_index,['gy']].values, (120))
		X_point[:,5] = np.reshape(gyro_data_df.loc[start_index:end_index,['gz']].values, (120))
		#print("OR:",X_point[0:5,4:5])
		#standardized (x-mean)/std
		#X_point[:,3:6] = (X_point[:,3:6] - gyro_mean ) / gyro_stddev
		if np.isnan(X_point).any():
			print("NaN")
		#print("STD: ",X_point[0:5,4:5])
		#X = raw_data_df[start_index:end_index].values
		#y_list.append(y)
		X.append(X_point)
		#print( "X point ", start_index," : ",end_index, " Shape: ", X.shape )
		start_index += step
		end_index += step
	return X

def predict_fragment(uuid, ts, norm, clf,  expected):
	#print(fragment[0], " ", fragment[3], " ", fragment[1], " ", fragment[4] )
	
	X = extractAccGyroSamples(uuid, ts, norm,step=60 )
	#print(np.shape(feats))
	X_list = []
	X_list.extend(X)
	#print(X_list)

	X_es = np.zeros((len(X_list),120,6))
	X_es[:] = [x for x in X_list]
	#scaling input
	X_es_scaled = np.zeros((len(y_list),120,6))
	scaler_X = extrautils.load_classifier("./GYRO_global_scaler_x")
	scaler_Y = extrautils.load_classifier("./GYRO_global_scaler_y")
	scaler_Z = extrautils.load_classifier("./GYRO_global_scaler_z")
	#Not scaling ACC [Ignatov 2018]
	X_es_scaled[:,:,0] = X_es[:,:,0]
	X_es_scaled[:,:,1] = X_es[:,:,1]
	X_es_scaled[:,:,2] = X_es[:,:,2]
	#Scaling gyroscope
	X_es_scaled[:,:,3] = scaler_X.transform(X_es[:,:,3])
	X_es_scaled[:,:,4] = scaler_Y.transform(X_es[:,:,4])
	X_es_scaled[:,:,5] = scaler_Z.transform(X_es[:,:,5])


	predictions = clf.predict(X_es_scaled, batch_size=1)
	predictions_inv = [ np.argmax(x) for x in predictions]
	#print(predictions)
	#print(predictions_inv)
	avg_value = np.mean(predictions,axis=0)
	avg_prediction = np.argmax(avg_value)
	#print("axis 0: ", avg_value )
	vote_count = [ predictions_inv.count(0), predictions_inv.count(1),predictions_inv.count(2),predictions_inv.count(3)]
	#print(vote_count)
	vote_prediction = np.argmax(vote_count)
	median_value = np.median(predictions,axis=0)
	median_pred = np.argmax(median_value)

	#print("axis 1: ",np.mean(predictions,axis=1) )
	#print("expected: ",expected, " avg: ", avg_prediction, " vote: ", vote_prediction, " median: ", median_pred)
	#input("pause")
	return avg_prediction, vote_prediction, median_pred




for fold in ["0"]:#,"1"]:#,"2","3","4"]:
	
	clf = Classifiers.HAR_CNN_IMU_4cl(patience=50,num_classes=4,kern_size=32,divide_kernel_size=True,suffix="f"+fold)
	clf.loadBestWeights()
	dataset_df = get_all_dataset(ts_users)
	predictions_avg = []
	predictions_vot = []
	predictions_med = []
	expecteds = []

	for index, fragment in dataset_df.iterrows():
		extrautils.printProgressBar(index, len(dataset_df), prefix = 'Progress:', suffix = 'Complete',length = 30)
		##check if data exists
		rawfile = rawdatafolder+fragment[0]+"/"+ str(fragment[2]) +".m_raw_acc.dat.gz"
		gyrofile = gyrodatafolder+fragment[0]+"/"+ str(fragment[2]) +".m_proc_gyro.dat.gz"
		if os.path.exists(gyrofile) and os.path.exists(rawfile) and (fragment[3] in [0,1,2,3]):
			average, vote, median = predict_fragment(fragment[0], fragment[2], normalized[fragment[0]], clf, fragment[3])
			expecteds.append(fragment[3])
			predictions_avg.append(average)
			predictions_vot.append(vote)
			predictions_med.append(median)
		else:
			print("RAW DATA NOT FOUND")
			print(rawfile)
			print(gyrofile)
			continue

	classes = ['Sit','Walk','Run','Cycle']#,'Stairs Up','Stairs Down']
	print("Results average prediction on fragment")
	clf.printConfusionMatrix(true=expecteds,pred=predictions_avg,classes=classes)
	cr = classification_report(np.array(expecteds), np.array(predictions_avg),target_names=classes,digits=5)
	print(cr)
	bal_acc = balanced_accuracy_score(np.array(expecteds), np.array(predictions_avg),adjusted=True)
	print("balanced Accuracy: ",bal_acc)
	#check median and majority voting
	print("Results majority voting prediction on fragment")
	clf.printConfusionMatrix(true=expecteds,pred=predictions_vot,classes=classes)
	cr = classification_report(np.array(expecteds), np.array(predictions_vot),target_names=classes,digits=5)
	print(cr)
	bal_acc = balanced_accuracy_score(np.array(expecteds), np.array(predictions_vot),adjusted=True)
	print("balanced Accuracy: ",bal_acc)
	#check median 
	print("Results median prediction on fragment")
	clf.printConfusionMatrix(true=expecteds,pred=predictions_med,classes=classes)
	cr = classification_report(np.array(expecteds), np.array(predictions_med),target_names=classes,digits=5)
	print(cr)
	bal_acc = balanced_accuracy_score(np.array(expecteds), np.array(predictions_med),adjusted=True)
	print("balanced Accuracy: ",bal_acc)

	y_results = np.zeros( (len(expecteds), 4) )
	y_results[:,0] = expecteds
	y_results[:,1] = predictions_avg
	y_results[:,2] = predictions_med
	y_results[:,3] = predictions_vot
	results_df = pd.DataFrame(y_results,columns=['Expected','Average','Median','MajVoting'])

	results_df.to_csv("./results_fragments.csv",index=False)
