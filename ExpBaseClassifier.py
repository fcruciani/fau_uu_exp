#ExpBaseClassifier
#Author: Federico Cruciani
#Description: Base class wrapping Keras and sklearn.
import numpy as np
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix, precision_recall_fscore_support, classification_report, accuracy_score#, balanced_accuracy_score
from keras.models import Sequential, load_model
from keras.callbacks import EarlyStopping, ModelCheckpoint, CSVLogger
from keras import backend as keras_backend
from keras.utils import plot_model
from keras.layers import *
from datetime import datetime
import keras
import seaborn as sn
from os.path import expanduser
import tensorflow as tf 
from keras.backend import tensorflow_backend as K 
#get actual home path for current user
home = expanduser("~")

basepath = './keras_logs/'



##Base class
class ExpBaseClassifier:
	def __init__(self,name,patience=25,fontSize=16):
		self.name = name
		font = {'family':'sans-serif', 'size':fontSize}
		matplotlib.rc('font',**font)
		#Init Multithreading
		RUN_NAME = name + str(datetime.utcnow())
		self.logger = keras.callbacks.TensorBoard( log_dir="logs/"+RUN_NAME+"/", write_graph=True )
		#stop criterion
		self.early_stopping = EarlyStopping(monitor='val_acc',patience=patience)
		self.csv_log_file = basepath+self.name+"_training_log.csv"
		self.csv_logger = CSVLogger(self.csv_log_file)
		#Checkpoint for weights
		#self.bestmodelweights = name+"weights_best_ep_{epoch:02d}_{val_acc:.3f}.hdf5"
		self.bestmodelweights = basepath+ name+"weights_best.hdf5"
		self.checkpoint = ModelCheckpoint(self.bestmodelweights, monitor='val_acc',verbose=1,save_best_only=True, mode='max')
		####Model init specialized in class
		

	def loadBestWeights(self):
		print(self.checkpoint)
		self.model.load_weights(self.bestmodelweights)
		#self.model.compile( loss='mse', optimizer='adam' )

	def predict(self,X_test,batch_size=1):
		predictions = self.model.predict(X_test,batch_size)
		return predictions

	def save(self):
		self.model.save(basepath+self.name+".h5")

	def load(self):
		self.model = load_model(basepath+self.name+".h5")

	
	def printConfusionMatrix(self,pred,true,classes):
		cm = confusion_matrix(np.array(true), np.array(pred) )
		cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
		print(cm)

	def reset_states(self):
		self.model.reset_states()

	def fit(self,X_tr,y_tr,X_vld,y_vld,batch_size=256,epochs=500,verbose=0):
		self.history = self.model.fit( X_tr, y_tr, validation_data=(X_vld,y_vld), batch_size=batch_size, epochs=epochs, shuffle=False, verbose=2, callbacks = [self.logger, self.early_stopping, self.checkpoint, self.csv_logger] )

	def fit_split_train_test(self,X_tr,y_tr,validation_split=0.1,batch_size=256,epochs=5000,verbose=0):
		self.history = self.model.fit( X_tr, y_tr, validation_split=validation_split,batch_size=batch_size, epochs=epochs, shuffle=False, verbose=2, callbacks = [self.logger, self.early_stopping, self.checkpoint, self.csv_logger] )

	def fit_generator(self,uuids,epochs,imu_bg):
		spe = imu_bg.get_steps_per_epoch(dict_uuids)
		self.history = self.model.fit_generator(bg.batch_generator(uuids,epochs=epochs),epochs=epochs,steps_per_epoch=spe)

	