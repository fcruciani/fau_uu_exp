import numpy as np
import pandas as pd 
#from extrautils import normalized, rawdatafolder, gyrodatafolder
import extrautils
import ExpClassifiers as Classifiers
import os
from sklearn.metrics import classification_report, balanced_accuracy_score

#RS_Datazet users all android -> all in m/s^2 not normalized [g]
normalized = {'user_0':False,'user_1':False,'user_2':False,'user_3':False,'user_4':False,'user_5':False,'user_6':False}

rawdatafolder = "/home/fedecrux/python/data/uu_dataset/raw_acc/"
gyrodatafolder = "/home/fedecrux/python/data/uu_dataset/raw_gyro/"

ts_users = ['user_0','user_1','user_2','user_3','user_4','user_5','user_6']


def get_all_dataset(uuids):
	gt_df = pd.read_csv("GROUNDTRUTH_RSDATASET.csv",names=['UUID','label','ts','class'])
	#exclude other uuids
	filtered_df = pd.DataFrame(columns=['UUID','label','ts','class'])
	for uuid in uuids:
		data_uuid = gt_df[ gt_df['UUID'] == uuid ]
		filtered_df = pd.concat([filtered_df,data_uuid], ignore_index=True)

	#print("all data: ",len(gt_df))
	#print("only uuids: ", len(filtered_df))
	df_sit_all = filtered_df[filtered_df['label'] == "SITTING" ] #0
	df_wal_all = filtered_df[filtered_df['label'] == "WALKING" ] #1
	df_run_all = filtered_df[filtered_df['label'] == "RUNNING" ] #2
	df_cyc_all = filtered_df[filtered_df['label'] == "CYCLING" ] #3
	#df_sup_all = filtered_df[filtered_df['label'] == "STAIRS_UP" ] #4
	#df_sdo_all = filtered_df[filtered_df['label'] == "STAIRS_DOWN" ] #5

	dataset_df = pd.concat([df_wal_all,df_sit_all,df_run_all,df_cyc_all], ignore_index=True)
	return dataset_df



def predict_fragment(uuid, ts, norm, clf, scaler, expected):
	#print(fragment[0], " ", fragment[3], " ", fragment[1], " ", fragment[4] )
	
	feats = extrautils.extractAccGyroFeatures(uuid, ts, norm,step=60,accdatapath=rawdatafolder,gyrodatapath=gyrodatafolder )
	#print(np.shape(feats))
	X_list = []
	X_list.extend(feats)
	#print(X_list)

	X_es = np.zeros((len(X_list),44))
	X_es[:] = [x for x in X_list]
	X_es_scaled = scaler.transform(X_es)
	predictions = clf.predict(X_es_scaled, batch_size=1)
	predictions_inv = [ np.argmax(x) for x in predictions]
	#print(predictions)
	#print(predictions_inv)
	avg_value = np.mean(predictions,axis=0)
	avg_prediction = np.argmax(avg_value)
	#print("axis 0: ", avg_value )
	vote_count = [ predictions_inv.count(0), predictions_inv.count(1),predictions_inv.count(2),predictions_inv.count(3)]
	#print(vote_count)
	vote_prediction = np.argmax(vote_count)
	median_value = np.median(predictions,axis=0)
	median_pred = np.argmax(median_value)

	#print("axis 1: ",np.mean(predictions,axis=1) )
	#print("expected: ",expected, " avg: ", avg_prediction, " vote: ", vote_prediction, " median: ", median_pred)
	#input("pause")
	return avg_prediction, vote_prediction, median_pred




for fold in ["0"]:#,"1"]:#,"2","3","4"]:
	
	clf = Classifiers.HAR_HCF_IMU(patience=100,suffix="f"+fold,num_classes=4)
	clf.loadBestWeights()
	scaler = extrautils.load_classifier("IMU_HCF_scaler")
	dataset_df = get_all_dataset(ts_users)
	predictions_avg = []
	predictions_vot = []
	predictions_med = []
	expecteds = []

	for index, fragment in dataset_df.iterrows():
		extrautils.printProgressBar(index, len(dataset_df), prefix = 'Progress:', suffix = 'Complete',length = 30)
		##check if data exists
		rawfile = rawdatafolder+fragment[0]+"/"+ str(fragment[2]) +".m_raw_acc.dat.gz"
		gyrofile = gyrodatafolder+fragment[0]+"/"+ str(fragment[2]) +".m_proc_gyro.dat.gz"
		if os.path.exists(gyrofile) and os.path.exists(rawfile) and (fragment[3] in [0,1,2,3]):
			average, vote, median = predict_fragment(fragment[0], fragment[2], normalized[fragment[0]], clf, scaler, fragment[3])
			expecteds.append(fragment[3])
			predictions_avg.append([average])
			predictions_vot.append([vote])
			predictions_med.append([median])
		else:
			print("RAW DATA NOT FOUND")
			print(rawfile)
			print(gyrofile)
			continue

	classes = ['Sit','Walk','Run','Cycle']#,'Stairs Up','Stairs Down']
	print("Results average prediction on fragment")
	clf.printConfusionMatrix(true=expecteds,pred=predictions_avg,classes=classes)
	cr = classification_report(np.array(expecteds), np.array(predictions_avg),target_names=classes,digits=5)
	print(cr)
	bal_acc = balanced_accuracy_score(np.array(expecteds), np.array(predictions_avg),adjusted=True)
	print("balanced Accuracy: ",bal_acc)
	#check median and majority voting
	print("Results majority voting prediction on fragment")
	clf.printConfusionMatrix(true=expecteds,pred=predictions_vot,classes=classes)
	cr = classification_report(np.array(expecteds), np.array(predictions_vot),target_names=classes,digits=5)
	print(cr)
	bal_acc = balanced_accuracy_score(np.array(expecteds), np.array(predictions_vot),adjusted=True)
	print("balanced Accuracy: ",bal_acc)
	#check median 
	print("Results median prediction on fragment")
	clf.printConfusionMatrix(true=expecteds,pred=predictions_med,classes=classes)
	cr = classification_report(np.array(expecteds), np.array(predictions_med),target_names=classes,digits=5)
	print(cr)
	bal_acc = balanced_accuracy_score(np.array(expecteds), np.array(predictions_med),adjusted=True)
	print("balanced Accuracy: ",bal_acc)